# Experiments parameters
 perf{
      testBuildingBlocks="false";
      PrivacyPreservation="true";
      microBench="false";
      MLAlgorithm="vfdt";
      runs_number ="1";
      thread_number="14";
      remoteClient="false";
      Incremental="true";
      OutputPath="/home/rania/CLionProjects/DAPPLE-2.0/EXP/MANU/";
      scenarioName ="fullCipherAdult";
      debug="true";
      maxTrainSize="2261";
      maxTestSize="500";
 }

 # The dataset configuration parameters
 dataset{
     mainPath = "/home/rania/CLionProjects/DAPPLE-2.0/";
     path="/home/rania/CLionProjects/DAPPLE-2.0/DATA/Datasets/Adult/FULL/";
     clearPath= "/home/rania/CLionProjects/DAPPLE-2.0/DATA/Datasets/Adult/FULL/";
     chunk_prefix="DO";
     class_number="2";
     dimension="16";
     chunk_number="1";
 }

 # The vfdt algorithm parameters
 vfdt{

     delta="0.000001";
     tau="1";
     grace="50";
     max_depth="30";
     threshold_number="2";
     maxNodes="1000";
 }

 # DTPKC parameters
 crypto{
     deserialize="false";
     pathSer="/home/rania/CLionProjects/DAPPLE-2.0/SER/";
     keySize ="1024";
     DTPKC_Err = "600";
     ComputationalPrecision="1000";
     useORE="false";
     sendParaClient="true";
     blindingSize="100";
     encryptData="true";
     delay="0";
     optim = "true";
     precompute = "true";
 }

 # Naive Bayes parameters
 nb{
     LaplaceConstant="10";
 }

 #LR parameters

 lr{
    alpha="10000";
    LRTH="0.5";
    batchSize="30";
    epochs="1";
    sgdWorkers="4";
 }

 # Network parameters
 network{
     portS="5001";
     serverIP="127.0.0.1";
 }