# Experiments parameters
 perf{
      testBuildingBlocks="false";
      PrivacyPreservation="true";
      MLAlgorithm="vfdt";
      runs_number ="1";
      thread_number="1";
      remoteClient="false";
      Incremental="true";
      OutputPath="/home/rania/CLionProjects/DAPPLE-2.0/EXP/DEBUG-VFDT/";
      scenarioName ="cipher";
      debug="true";
      maxTrainSize="100";
      maxTestSize="400";
 }

 # The dataset configuration parameters
 dataset{
     mainPath = "/home/rania/CLionProjects/DAPPLE-2.0/";
     path="/home/rania/CLionProjects/DAPPLE-2.0/DATA/Datasets/Adult/";
     clearPath= "/home/rania/CLionProjects/DAPPLE-2.0/DATA/Datasets/Adult/";
     chunk_prefix="DO";
     class_number="2";
     dimension="15";
     chunk_number="1";
 }

 # The vfdt algorithm parameters
 vfdt{
     delta="0.000001";
     tau="1";
     grace="15";
     max_depth="14";
     threshold_number="2";
 }

 # DTPKC parameters
 crypto{
     deserialize="false";
     pathSer="/home/rania/CLionProjects/DAPPLE-2.0/SER/";
     keySize ="512";
     DTPKC_Err = "600";
     ComputationalPrecision="1000";
     useORE="false";
     sendParaClient="true";
     blindingSize="200";
     encryptData="true";
 }

 # Naive Bayes parameters
 nb{
     LaplaceConstant="1";
 }

 #LR parameters

 lr{
    alpha="10000";
    LRTH="0.5";
    batchSize="30";
    epochs="2";
 }

 # Network parameters
 network{
     portS="5001";
     serverIP="127.0.0.1";
 }