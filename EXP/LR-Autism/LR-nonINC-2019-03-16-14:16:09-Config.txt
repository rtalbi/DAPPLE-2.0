# Experiments parameters
 perf{
      testBuildingBlocks="false";
      PrivacyPreservation="false";
      MLAlgorithm="lr";
      runs_number ="1";
      thread_number="16";
      remoteClient="false";
      Incremental="false";
      OutputPath="/home/rania/CLionProjects/DAPPLE-2.0/EXP/LR-Autism/";
      scenarioName ="LR-nonINC";
      debug="true";
      maxTrainSize="200";
      maxTestSize="400";
 }

 # The dataset configuration parameters
 dataset{
     mainPath = "/home/rania/CLionProjects/DAPPLE-2.0/";
     path="/home/rania/CLionProjects/DAPPLE-2.0/DATA/Datasets/Autism/LR/";
     clearPath= "/home/rania/CLionProjects/DAPPLE-2.0/DATA/Datasets/Autism/LR/";
     chunk_prefix="DO";
     class_number="2";
     dimension="21";
     chunk_number="8";
 }

 # The vfdt algorithm parameters
 vfdt{
     delta="0.000001";
     tau="1";
     grace="2";
     max_depth="6";
     threshold_number="2";
 }

 # DTPKC parameters
 crypto{
     deserialize="false";
     pathSer="/home/rania/CLionProjects/DAPPLE-2.0/SER/";
     keySize ="512";
     DTPKC_Err = "600";
     ComputationalPrecision="1000";
     useORE="false";
     sendParaClient="true";
     blindingSize="300";
     encryptData="true";
 }

 # Naive Bayes parameters
 nb{
     LaplaceConstant="1";
 }

 #LR parameters

 lr{
    alpha="10000";
    LRTH="0.5";
    batchSize="30";
    epochs="2";
 }

 # Network parameters
 network{
     portS="5001";
     serverIP="127.0.0.1";
 }