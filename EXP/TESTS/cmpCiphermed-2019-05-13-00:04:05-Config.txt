# Experiments parameters
 perf{
      testBuildingBlocks="false";
      PrivacyPreservation="true";
      MLAlgorithm="nb";
      runs_number ="1";
      thread_number="4";
      remoteClient="false";
      Incremental="false";
      OutputPath="/home/rania/CLionProjects/DAPPLE-2.0/EXP/TESTS/";
      scenarioName ="cmpCiphermed";
      debug="false";
      maxTrainSize="100";
      maxTestSize="400";
 }

 # The dataset configuration parameters
 dataset{
     mainPath = "/home/rania/CLionProjects/DAPPLE-2.0/";
     path="/home/rania/CLionProjects/DAPPLE-2.0/DATA/Datasets/Nursery2/";
     clearPath= "/home/rania/CLionProjects/DAPPLE-2.0/DATA/Datasets/Nursery2/";
     chunk_prefix="DO";
     class_number="5";
     dimension="9";
     chunk_number="1";
 }

 # The vfdt algorithm parameters
 vfdt{
     delta="0.000001";
     tau="1";
     grace="50";
     max_depth="2";
     threshold_number="2";
     maxNodes="45";
 }

 # DTPKC parameters
 crypto{
     deserialize="false";
     pathSer="/home/rania/CLionProjects/DAPPLE-2.0/SER/";
     keySize ="1024";
     DTPKC_Err = "600";
     ComputationalPrecision="1000";
     useORE="false";
     sendParaClient="true";
     blindingSize="100";
     encryptData="true";
 }

 # Naive Bayes parameters
 nb{
     LaplaceConstant="1";
 }

 #LR parameters

 lr{
    alpha="10000";
    LRTH="0.5";
    batchSize="30";
    epochs="2";
 }

 # Network parameters
 network{
     portS="5001";
     serverIP="127.0.0.1";
 }