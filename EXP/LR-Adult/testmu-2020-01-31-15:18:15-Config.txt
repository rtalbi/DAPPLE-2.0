# Experiments parameters
 perf{
      testBuildingBlocks="false";
      PrivacyPreservation="true";
      microBench="true";
      MLAlgorithm="lr";
      runs_number ="1";
      thread_number="16";
      remoteClient="false";
      Incremental="false";
      OutputPath="/home/rania/CLionProjects/DAPPLE-2.0/EXP/LR-Adult/";
      scenarioName ="testmu";
      debug="true";
      maxTrainSize="20";
      maxTestSize="20";
 }

 # The dataset configuration parameters
 dataset{
     mainPath = "/home/rania/CLionProjects/DAPPLE-2.0/";
     path="/home/rania/CLionProjects/DAPPLE-2.0/DATA/Datasets/Adult/LR/";
     clearPath= "/home/rania/CLionProjects/DAPPLE-2.0/DATA/Datasets/Adult/LR/";
     chunk_prefix="DO";
     class_number="2";
     dimension="15";
     chunk_number="16";
 }

 # The vfdt algorithm parameters
 vfdt{
     delta="0.000001";
     tau="1";
     grace="2";
     max_depth="6";
     threshold_number="2";
     maxNodes="50";
 }

 # DTPKC parameters
 crypto{
     deserialize="true";
     pathSer="/home/rania/CLionProjects/DAPPLE-2.0/SER/";
     keySize ="512";
     DTPKC_Err = "600";
     ComputationalPrecision="1000";
     useORE="false";
     sendParaClient="true";
     blindingSize="300";
     encryptData="true";
     delay="1";
 }

 # Naive Bayes parameters
 nb{
     LaplaceConstant="1";
 }

 #LR parameters

 lr{
    alpha="100";
    LRTH="0.5";
    batchSize="100";
    epochs="3";
 }

 # Network parameters
 network{
     portS="5001";
     serverIP="127.0.0.1";
 }