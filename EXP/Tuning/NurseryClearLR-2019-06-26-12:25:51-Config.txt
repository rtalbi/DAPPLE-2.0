# Experiments parameters
 perf{
      testBuildingBlocks="false";
      PrivacyPreservation="false";
      MLAlgorithm="lr";
      runs_number ="1";
      thread_number="14";
      remoteClient="false";
      Incremental="true";
      OutputPath="/home/rania/CLionProjects/DAPPLE-2.0/EXP/Tuning/";
      scenarioName ="NurseryClearLR";
      debug="true";
      maxTrainSize="10";
      maxTestSize="10";
 }

 # The dataset configuration parameters
 dataset{
     mainPath = "/home/rania/CLionProjects/DAPPLE-2.0/";
     path="/home/rania/CLionProjects/DAPPLE-2.0/DATA/Datasets/Nursery/LR/";
     clearPath= "/home/rania/CLionProjects/DAPPLE-2.0/DATA/Datasets/Nursery/LR/";
     chunk_prefix="DO";
     class_number="5";
     dimension="9";
     chunk_number="10";
 }

 # The vfdt algorithm parameters
 vfdt{
     delta="0.000001";
     tau="1";
     grace="50";
     max_depth="10";
     threshold_number="2";
     maxNodes="100";
 }

 # DTPKC parameters
 crypto{
     deserialize="false";
     pathSer="/home/rania/CLionProjects/DAPPLE-2.0/SER/";
     keySize ="1024";
     DTPKC_Err = "600";
     ComputationalPrecision="1000";
     useORE="false";
     sendParaClient="true";
     blindingSize="100";
     encryptData="true";
 }

 # Naive Bayes parameters
 nb{
     LaplaceConstant="1";
 }

 #LR parameters

lr{
    alpha="100";
    LRTH="0.5";
    batchSize="100";
    epochs="1";
 }

 # Network parameters
 network{
     portS="5001";
     serverIP="127.0.0.1";
 }