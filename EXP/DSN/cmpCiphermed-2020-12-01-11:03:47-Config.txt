# Experiments parameters
 perf{
      testBuildingBlocks="false";
      PrivacyPreservation="false";
      microBench="false";
      MLAlgorithm="vfdt";
      runs_number ="1";
      thread_number="4";
      remoteClient="false";
      Incremental="false";
      OutputPath="/home/rania/CLionProjects/DAPPLE-2.0/EXP/DSN/";
      scenarioName ="cmpCiphermed";
      debug="false";
      maxTrainSize="100";
      maxTestSize="400";
 }

 # The dataset configuration parameters
 dataset{
     mainPath = "/home/rania/CLionProjects/DAPPLE-2.0/";
     path="/home/rania/CLionProjects/DAPPLE-2.0/DATA/Datasets/Nursery/";
     clearPath= "/home/rania/CLionProjects/DAPPLE-2.0/DATA/Datasets/Nursery/";
     chunk_prefix="DO";
     class_number="5";
     dimension="9";
     chunk_number="1";
 }

 # The vfdt algorithm parameters
 vfdt{
     delta="0.000001";
     tau="1";
     grace="50";
     max_depth="20";
     threshold_number="3";
     maxNodes="500";
 }

 # DTPKC parameters
 crypto{
     deserialize="false";
     pathSer="/home/rania/CLionProjects/DAPPLE-2.0/SER/";
     keySize ="1024";
     DTPKC_Err = "600";
     ComputationalPrecision="1000";
     useORE="false";
     sendParaClient="true";
     blindingSize="100";
     encryptData="true";
     delay="0";
 }

 # Naive Bayes parameters
 nb{
     LaplaceConstant="10";
 }

 #LR parameters

 lr{
    alpha="10000";
    LRTH="0.5";
    batchSize="30";
    epochs="2";
    sgdWorkers="4";
 }

 # Network parameters
 network{
     portS="5001";
     serverIP="127.0.0.1";
 }