# Experiments parameters
 perf{
      testBuildingBlocks="false";
      PrivacyPreservation="false";
      microBench="true";
      MLAlgorithm="lr";
      runs_number ="1";
      thread_number="14";
      remoteClient="false";
      Incremental="false";
      OutputPath="/home/rania/CLionProjects/DAPPLE-2.0/DSN2020/";
      scenarioName ="SpamTuning";
      debug="false";
      maxTrainSize="100";
      maxTestSize="10";
 }

 # The dataset configuration parameters
 dataset{
     mainPath = "/home/rania/CLionProjects/DAPPLE-2.0/";
     path="/home/rania/CLionProjects/DAPPLE-2.0/DATA/Datasets/Spam/";
     clearPath= "/home/rania/CLionProjects/DAPPLE-2.0/DATA/Datasets/Spam/";
     chunk_prefix="DO";
     class_number="2";
     dimension="58";
     chunk_number="16";
 }

 # The vfdt algorithm parameters
 vfdt{
     delta="0.000001";
     tau="1";
     grace="50";
     max_depth="10";
     threshold_number="2";
     maxNodes="100";
 }

 # DTPKC parameters
 crypto{
     deserialize="false";
     pathSer="/home/rania/CLionProjects/DAPPLE-2.0/SER/";
     keySize ="1024";
     DTPKC_Err = "600";
     ComputationalPrecision="1000";
     useORE="false";
     sendParaClient="true";
     blindingSize="100";
     encryptData="true";
     delay="2";
 }

 # Naive Bayes parameters
 nb{
     LaplaceConstant="1";
 }

 #LR parameters

lr{
    alpha="1";
    LRTH="0.5";
    batchSize="64";
    epochs="1";
 }


 # Network parameters
 network{
     portS="5001";
     serverIP="127.0.0.1";
 }