#!/bin/sh
# 5 runs of PPLR ( thread variation + single core thread variation )

# Vary number of threads

cpt=$threads
while [ $cpt -gt 0 ]
do
echo running with $cpt threads
./DAPPLE $cpt
cpt=$(( $cpt - 2 ))
done

# Vary number of threads for a single core

cpt=$threads
while [ $cpt -gt 0 ]
do
echo running with $cpt threads
taskset 1 ./DAPPLE $cpt
cpt=$(( $cpt - 2 ))
done
