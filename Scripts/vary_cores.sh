#!/bin/sh

maxCores=$1
maxCores= expr $maxCores + 1
threads=$2
cpt=1
list=1

while [ $cpt -lt $maxCores ]
do
echo running with $cpt cores...
cmd="taskset --cpu-list $list ../cmake-build-debug/DAPPLE $threads"
echo $cmd
#bash -C $cmd
cpt=$(( $cpt + 1 ))
list=$list",$cpt"
done