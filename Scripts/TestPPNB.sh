#!/bin/sh
#5 runs of PPNB

maxCores=$1
maxCores= expr $maxCores + 1
threads=$2

./DAPPLE $threads
./DAPPLE $threads
./DAPPLE $threads
./DAPPLE $threads
./DAPPLE $threads

# Vary cores

cpt=2
list=2

while [ $cpt -lt $maxCores ]
do
echo running with $cpt cores...
taskset --cpu-list $list ./DAPPLE $threads"
cpt=$(( $cpt + 2 ))
list=$list",$cpt"
done

# Vary number of threads

cpt=$threads
while [ $cpt -gt 0 ]
do
echo running with $cpt threads
./DAPPLE $cpt
cpt=$(( $cpt - 2 ))
done

# Vary number of threads for a single core

cpt=$threads
while [ $cpt -gt 0 ]
do
echo running with $cpt threads
taskset 1 ./DAPPLE $cpt
cpt=$(( $cpt - 2 ))
done



