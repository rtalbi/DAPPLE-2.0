#!/bin/sh
#5 runs of PPLR ( Number of cores variation + 5 runs )

maxCores=$1
maxCores= expr $maxCores + 1
threads=$2

./DAPPLE $threads
./DAPPLE $threads
./DAPPLE $threads
./DAPPLE $threads
./DAPPLE $threads

# Vary cores

cpt=2
cpt2=1
list=1,2

while [ $cpt -lt $maxCores ]
do
echo running with $cpt cores...
taskset --cpu-list $list ./DAPPLE $threads
cpt=$(( $cpt + 2 ))
cpt2=$(( $cpt2 + 2 ))
list=$list",$cpt2,$cpt"
echo $list
done

# Vary number of threads

cpt=$threads
while [ $cpt -gt 0 ]
do
echo running with $cpt threads
./DAPPLE $cpt
cpt=$(( $cpt - 2 ))
done

# Vary number of threads for a single core

cpt=$threads
while [ $cpt -gt 0 ]
do
echo running with $cpt threads
taskset 1 ./DAPPLE $cpt
cpt=$(( $cpt - 2 ))
done


cd ../../PPLR
git add .
git commit -m "New tests PPLR"
git checkout -b testPPLR
git push origin testPPLR