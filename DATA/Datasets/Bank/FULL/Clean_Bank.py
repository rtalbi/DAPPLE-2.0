""" This code allows to prepare the dataset bank for the training by replacing
textual values with integers and by multiplying all integers by a certain value

 run python3 Clean_Bank.py

 """


import pandas as pd



# read the dataset
df = pd.read_csv("data-brute.txt", header=None, sep=';' , low_memory=False)




# Make all attributes integers

for c in df.columns:
    if(df[c].dtype == 'object'):
        df[c]=df[c].str.strip()
        df[c]= df[c].astype('category')
        df[c] = df[c].cat.codes

#Multiply all values by 100 to get a precision of 10^2
for c in df.columns:
    df[c]= df[c].astype(int)


df[0] = df[0].apply(lambda x: round(x/10))

df[5] = df[5].apply(lambda x: round(x/100))

df[5] = df[5].clip(lower=0)

df[11] = df[11].apply(lambda x: round(x/100))


df[13] = df[13].apply(lambda x: round(x/100))

df[13] = df[13].clip(lower=0)



#df[16] = df[16].apply(lambda x: (x%2))
# replace -1 by 0 (no pdays)

name="./bank_full.csv"
df.to_csv(name, sep=',', header=False, index=False)



